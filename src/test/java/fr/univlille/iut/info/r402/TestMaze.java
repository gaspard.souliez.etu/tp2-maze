package fr.univlille.iut.info.r402;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMaze {
    @Test
    void should_initialize_one_cell_maze() {
        String expectedRender1x1 = """
            ####
            #  #
            ####""";
        assertEquals(expectedRender1x1, Maze.of(1,1).render());
    }

    @Test
    void should_initialize_maze_with_size_of_empty_cell() {
        String expectedRender2x2 = """
            #######
            #  #  #
            #######
            #  #  #
            #######""";
        assertEquals(expectedRender2x2, Maze.of(2,2).render());
    }

}
