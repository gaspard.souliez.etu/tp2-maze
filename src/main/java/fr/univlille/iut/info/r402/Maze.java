package fr.univlille.iut.info.r402;

public class Maze {
    private int row;
    private int col;

    public Maze(int row, int col){
        this.row = row;
        this.col = col;
    }

    public static Maze of(int row, int col){
        return new Maze(row*2+1, col*3+1);
    }

    public String render(){
        StringBuilder maze = new StringBuilder();
        for(int i=0; i<this.row; i++){
            for(int y=0; y<this.col;y++){
                if(i%2 == 0 ||y%3 == 0){
                    maze.append("#");
                }else{
                    maze.append(" ");
                }
            }
            if(i<this.row-1) {
                maze.append("\n");
            }
        }
        return maze.toString();
    }
}
